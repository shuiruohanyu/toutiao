/*!
 * Koa CMS Backstage management
 *
 * Copyright JS suwenhao
 * Released under the ISC license
 * Email swh1057607246@qq.com
 *
 */
const user = require("./user"); // 用户接口
const channels = require('./channels')
const articles = require('./articles')
const article = require('./article')
const comments = require('./comments')
const comment = require('./comment')
const authorizations = require('./authorizations')
const search = require('./search')
const suggestion = require('./suggestion')
const sms = require('./sms')
const mp = require('./mp')
const address = require('../../../address')

const multer = require("koa-multer");
//配置
//文件上传限制
const limits = {
  fields: 1,//非文件字段的数量
  fileSize: 1024 * 1024,//文件大小 单位 b
  files: 1//文件数量
}
let storage = multer.diskStorage({
  //文件保存路径
  destination: function (req, file, cb) {
    cb(null, "public/uploads/"); //注意路径必须存在
  },
  //修改文件名称
  filename: function (req, file, cb) {
    let fileFormat = file.originalname.split(".");
    cb(null, Date.now() + "." + fileFormat[fileFormat.length - 1]);
  },
});
// 文件过滤器
const fileFilter = (req, file, cb) => {
  
  // 这个函数应该调用 `cb` 用boolean值来
  // 指示是否应接受该文件
  const fileName = file.originalname  // 获取文件名
  const names = fileName.split('.').reverse()
  const imageType = ['jpg', 'gif', 'jpeg', 'png']
  if (names.length && names.some(item => imageType.some(obj => obj === item.toLowerCase()))) {
    cb(null, true)
  }else {
    cb(new Error('当前服务上传只支持图片类型jpg/gif/jpeg/png'))
  }
  // 拒绝这个文件，使用`false`，像这样:
  // cb(null, false)

  // 接受这个文件，使用`true`，像这样:

  // 如果有问题，你可以总是这样发送一个错误:
  // cb(new Error('I don\'t have a clue!'))

}
//加载配置
let upload = multer({ storage, limits, fileFilter });
const router = require('koa-router')()

 // 多级匹配 用户模块
router.use('/user', user)

router.use('/mp', mp)

router.use('/channels', channels)

router.use('/articles', articles)

router.use('/article', article)

router.use('/comments', comments)

router.use('/comment', comment)

router.use('/authorizations', authorizations)

router.use('/search', search)

router.use('/suggestion', suggestion)

router.use('/sms', sms)

router.get('/announcements', async ctx => {
  return  {
      data : {
        total_count: 3,
        page: 1, 
        per_page: 10,
        results : [{
            id: 1,
            title: '黑马程序员招新啦, 诚招各路编程高手',
            pubdate: '2021/05/25 16:52:39'
        },{
            id: 2,
            title: '传智教育成为A股中第一支成功IPO的教育产业股',
            pubdate: '2021/1/25 16:52:39'
        },{
            id: 3,
            title: 'web前端工程师训练营来袭, 小伙伴们踊跃报名',
            pubdate: '2021/02/25 08:52:39'
        }]
      }
  }
})
// 上传头像
router.post(
  "/upload",
  upload.fields([{ name: "image", maxCount: 1 }]),
  async (ctx) => {
    const addressPath = process.env.AddressType == 1 ? address.address : `${address.host}:${address.port}`
    const { image } = ctx.req.files;
    let filePath = "";
    if (image && image.length) {
      filePath = `http://${addressPath}/uploads/` + image[0].filename;
    }
    ctx.status = 200
    return { data: { url: filePath } };
  }
);
module.exports = router.routes();
