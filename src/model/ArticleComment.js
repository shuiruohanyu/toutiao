/*!
 * Koa CMS Backstage management
 *
 * Copyright JS suwenhao
 * Released under the ISC license
 * Email shuiruohanyu@foxmail.com
 *
 */
var mongoose = require('mongoose')
const sd = require('silly-datetime')

var schema = new mongoose.Schema({
    "comment_id" : { type: String },
	"user_id" : { type: String },
	"article_id" : { type: String },
	"parent_id" : { type: String },
	"like_count" : { type: Number, default: 0 },
	"reply_count" : { type: Number, default: 0 },
	"content" :{ type: String },
	"is_top" : { type: String },
	"status" : { type: String },
	"create_time" : { type: String, default:() =>  sd.format(new Date(), 'YYYY-MM-DD HH:mm:ss') }
})
var ArticleComment = mongoose.model('news_comment', schema, 'news_comment'); 

module.exports = ArticleComment;