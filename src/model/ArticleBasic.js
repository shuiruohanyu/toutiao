/*!
 * Koa CMS Backstage management
 *
 * Copyright JS suwenhao
 * Released under the ISC license
 * Email shuiruohanyu@foxmail.com
 *
 */
var mongoose = require('mongoose')
const sd = require('silly-datetime')

var schema = new mongoose.Schema({
	"content" : { type: String },
    "article_id" :{ type: String  },
	"user_id" : { type: String },
	"channel_id" : { type: String  },
	"title" : { type: String },
	"cover" : { type: String },
	"is_advertising" : { type: String },
	"create_time" : { type: String, default:() =>  sd.format(new Date(), 'YYYY-MM-DD HH:mm:ss')  },
	"status" : { type: String },
	"reviewer_id" : { type: String },
	"review_time" : { type: String, default:() =>  sd.format(new Date(), 'YYYY-MM-DD HH:mm:ss') },
	"delete_time" : { type: String, default:() =>  sd.format(new Date(), 'YYYY-MM-DD HH:mm:ss') },
	"comment_count" :{ type:  Number, default: 0 },
	"allow_comment" : { type: String },
	"update_time" : { type: String , default:() =>  sd.format(new Date(), 'YYYY-MM-DD HH:mm:ss')},
	"reject_reason" : { type: String },
	"read_count": { type: Number, default: 0 }
})
var ArticleBasic = mongoose.model('news_article_basic', schema, 'news_article_basic'); 

module.exports = ArticleBasic;