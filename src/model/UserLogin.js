/*!
 * Koa CMS Backstage management
 *
 * Copyright JS suwenhao
 * Released under the ISC license
 * Email shuiruohanyu@foxmail.com
 *
 */
var mongoose = require('mongoose')
const sd = require('silly-datetime')

var schema = new mongoose.Schema({
	"user_id" : { type: String, index: true },
	"token" : { type: String },
	"refresh_token" :{ type: String },
	"create_time" : { type: String , default: () =>  sd.format(new Date(), 'YYYY-MM-DD HH:mm:ss') },
     "timer": { type: Number } // 时间戳
})
var UserLogin = mongoose.model('user_login', schema, 'user_login'); 

module.exports = UserLogin