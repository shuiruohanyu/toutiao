/*!
 * Koa CMS Backstage management
 *
 * Copyright JS suwenhao
 * Released under the ISC license
 * Email shuiruohanyu@foxmail.com
 *
 */
var mongoose = require('mongoose')

var schema = new mongoose.Schema({
    "channel_id" : { type: String },
	"channel_name" : { type: String },
	"create_time" : { type: String },
	"update_time" : { type: String },
	"sequence" : { type: String },
	"is_visible" : { type: String },
	"is_default" : { type: String }
})
var Channel = mongoose.model('news_channel', schema, 'news_channel'); 

module.exports = Channel;