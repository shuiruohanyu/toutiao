/*!
 * Koa CMS Backstage management
 *
 * Copyright JS suwenhao
 * Released under the ISC license
 * Email shuiruohanyu@foxmail.com
 *
 */
var mongoose = require('mongoose')
const sd = require('silly-datetime')

var schema = new mongoose.Schema({
    "message_id": {type: String},
	"user_id" : { type: String },
    "type" : { type: String },
    "title" : { type: String },
	"content" : { type: String },
    "comment_target" : { type: String },
    "comment_content" : { type: String },
	"create_time" : { type: String , default:() =>  sd.format(new Date(), 'YYYY-MM-DD HH:mm:ss') },
})
var UserMessage = mongoose.model('user_message', schema, 'user_message'); 

module.exports = UserMessage