process.env.AddressType = 1 // 设置生产环境变量

/*!
 * Koa CMS Backstage management
 *
 * Copyright JS suwenhao
 * Released under the ISC license
 * Email swh1057607246@qq.com
 *
 */
const Koa = require("koa"),
  Router = require("koa-router"),
  Static = require("koa-static"),
  // Session = require("koa-session"),
  BodyParser = require("koa-bodyparser"),
  path = require("path"),
  compress = require("koa-compress"),
  jsonp = require("koa-jsonp"),
  json = require("./src/routes/json"),
  api = require("./src/routes/api"), // 后端接口
  url = require("url"),
  opn = require("opn"),
  cors = require("koa2-cors"),
  address = require("./address"),
  service = require("./src/bury"),
  { blackList } = require("./src/routes/constant"),
  { checkTokenByContext } = require("./src/routes/token")
require("./src/db") // 引入数据库
const myIO = require("./src/routes/websocket")
// 初始化web服务
const app = new Koa()
var server = require("http").createServer(app.callback())
var APICount = require("./src/model/ApiCount")
const sd = require("silly-datetime")

const router = new Router()
//配置session
app.keys = ["some secret hurr"]
app.use(cors()) // 注册使用跨域中间件

// app.use(
//   Session({
//       key: "koa:sess",
//       maxAge: 5400000,
//       overwrite: true,
//       httpOnly: true,
//       signed: true,
//       rolling: true,
//       renew: false,
//     },app)
// );
// 注册redis的中间件
// app.use(koaSession({
//   store: new Redis() // 实例化redis
// }));
//配置静态资源
app.use(Static(path.join(__dirname, "public")))
app.use(Static(path.join(__dirname, "statics")))
//配置post请求数据接收
app.use(BodyParser())
//jsonp
app.use(jsonp())

//gzipd
app.use(
  compress({
    filter: function (content_type) {
      return true
    },
    threshold: 2048,
    flush: require("zlib").Z_SYNC_FLUSH,
  })
)
// //全局属性
app.use(async (ctx, next) => {
  // if (!isBuryStart) {
  //   // 只记录第一次埋点
  //   isBuryStart = true
  //   service.bury(ctx, 'toutiao')
  // }
  const prefix = "/v1_0/"
  var pathname = url.parse(ctx.url).pathname
  // 记录
  if (pathname !== "/api_count") {
    APICount.create({
      api_host: ctx.request.header.host,
      api_method: ctx.request.method,
      api_url: pathname,
      api_name: pathname,
      user_agent: ctx.request.header["user-agent"] || "",
    })
  }
  if (!blackList.some((item) => prefix + item === pathname)) {
    // 如果在白名单里面 就放过 不检查token有效性
    await next()
  } else {
    const isOut = await checkTokenByContext(ctx) // 是否超时
    if (isOut) {
      await next()
    } else {
      ctx.status = 401 // 超时token
      ctx.body = { message: "token超时或者未传token" }
    }
  }
})

app.use(async (ctx, next) => {
  try {
    const data = await next() // 获取接口返回数据
    if (data && !data.success && data.status) {
      ctx.status = data.status
    }
    ctx.body = { ...json, ...data, status: undefined, success: undefined }
    // 进行数据埋点  记录操作的信息
  } catch (error) {
    // 进行数据埋点  记录操作的信息
    service.bury(ctx, "toutiao", error.message)
    console.log("执行接口出现异常:" + error.message)
    ctx.status = 500
    ctx.body = {
      message: error.message,
    }
  }
})

// 启动入口
// 保证加不加app都可以正常启动
router.use("/v1_0", api)
// 获取访问的数据集合
router.get("/api_count", async (ctx) => {
  // 请求总次数
  const allCount = await APICount.find().countDocuments() // 总请求次数
  // // 只计算12小时之内的请求数据
  // var currentTime = new Date()
  // var time = new Date(currentTime.setHours(currentTime.getHours() - 1))
  // const result = await APICount.aggregate([{
  //   $match: {
  //     "create_time": {
  //       "$gt": sd.format(time, 'YYYY-MM-DD HH:mm:ss')
  //     }
  //   }
  // },{
  //   $group: {"_id":'$create_time', 'api_count': { "$sum": 1 } }
  // },{
  //   $sort: { "_id": 1 }
  // }])
  return { allCount }
})
// router.use
// router.use('/appy/v1_1/articles', article)
// router.use('/v1_0', api)
// 保证加不加app都可以正常启动
app.use(router.routes())

//启动路由
app.use(router.allowedMethods())

myIO.start(server)

app.use(async (ctx) => {
  console.log(ctx)
  if (ctx.status === 404) {
    console.log("找不到接口:" + ctx.originalUrl)
    service.bury(ctx, "toutiao", "找不到接口地址:" + ctx.originalUrl) // 记录地址错误的问题
    ctx.status = 404
    ctx.body = {
      message: "您请求的接口还未实现, 请检查您的请求路径是否正确!",
    }
  }
})

//启动服务器
server.listen(address.port, (err) => {
  console.log(`黑马头条后端接口启动, http://${address.host}:${address.port}`)
  opn(`http://${address.host}:${address.port}/index.html`)
})

setInterval(() => myIO.sendCurrentPeople(), 3000)
